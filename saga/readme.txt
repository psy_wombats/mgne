================================================================================
= SAGA v. 20140207  -not intended for public release-                          =
= running on MGNENGINE v. 20140207                                             =
= contact: aking (psy_wombats@wombatrpgs.net)                                  =
================================================================================

TO RUN:
Double-click game.bat. If you're not running windows, feel free to run the
executable jar also provided.

REPORTING PROBLEMS:
The game produces an info.log and error.log. Send these to our bug collector,
psy_wombats@wombatrpgs.net.

INFO:
There isn't anything in-game right now. Just a snapshot of what I've got so far.

CONTROLS:
Arrow keys.