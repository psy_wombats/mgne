<?xml version="1.0" encoding="UTF-8"?>
<tileset name="lower" tilewidth="16" tileheight="16">
 <image source="lower2.PNG" width="112" height="256"/>
 <tile id="3">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="x" value=""/>
  </properties>
  <animation>
   <frame tileid="56" duration="500"/>
   <frame tileid="63" duration="500"/>
   <frame tileid="70" duration="500"/>
   <frame tileid="77" duration="500"/>
  </animation>
 </tile>
 <tile id="58">
  <animation>
   <frame tileid="59" duration="500"/>
   <frame tileid="60" duration="500"/>
   <frame tileid="61" duration="500"/>
   <frame tileid="62" duration="500"/>
  </animation>
 </tile>
 <tile id="63">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="x" value=""/>
  </properties>
  <animation>
   <frame tileid="84" duration="500"/>
   <frame tileid="91" duration="500"/>
   <frame tileid="98" duration="500"/>
   <frame tileid="105" duration="500"/>
  </animation>
 </tile>
 <tile id="91">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="98">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="x" value=""/>
  </properties>
 </tile>
</tileset>
