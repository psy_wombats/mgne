/**
 *  WarheadType.java
 *  Created on Feb 24, 2014 7:39:32 PM for project tactics-schema
 *  Author: psy_wombats
 *  Contact: psy_wombats@wombatrpgs.net
 */
package net.wombatrpgs.tacticsschema.rpg.abil.data;

/**
 * Just a hookup for in-game effect.
 */
public enum WarheadType {
	
	MELEE,

}
