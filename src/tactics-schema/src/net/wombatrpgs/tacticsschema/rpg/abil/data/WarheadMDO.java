/**
 *  WarheadMDO.java
 *  Created on Feb 24, 2014 10:10:32 PM for project tactics-schema
 *  Author: psy_wombats
 *  Contact: psy_wombats@wombatrpgs.net
 */
package net.wombatrpgs.tacticsschema.rpg.abil.data;

import net.wombatrpgs.mgns.core.MainSchema;
import net.wombatrpgs.mgns.core.Annotations.ExcludeFromTree;

/**
 * Abileffect substitute.
 */
@ExcludeFromTree
public class WarheadMDO extends MainSchema {

}
