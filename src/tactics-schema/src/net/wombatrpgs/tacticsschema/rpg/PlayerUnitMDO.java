/**
 *  PlayerUnitMDO.java
 *  Created on Feb 12, 2014 10:14:10 PM for project tactics-schema
 *  Author: psy_wombats
 *  Contact: psy_wombats@wombatrpgs.net
 */
package net.wombatrpgs.tacticsschema.rpg;

import net.wombatrpgs.mgns.core.Annotations.Path;

/**
 * Human-controlled game units.
 */
@Path("rpg/")
public class PlayerUnitMDO extends GameUnitMDO {

}
