/**
 *  FlagEntryMDO.java
 *  Created on Feb 28, 2014 6:55:51 PM for project tactics-schema
 *  Author: psy_wombats
 *  Contact: psy_wombats@wombatrpgs.net
 */
package net.wombatrpgs.tacticsschema.rpg.data;

import net.wombatrpgs.mgns.core.HeadlessSchema;

/**
 * Indicates that a statset carries a flag.
 */
public class FlagEntryMDO extends HeadlessSchema {
	
	public Flag flag;

}
