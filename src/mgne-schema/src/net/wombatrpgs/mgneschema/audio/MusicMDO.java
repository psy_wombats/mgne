/**
 *  MusicMDO.java
 *  Created on Mar 24, 2013 2:18:47 AM for project RainfallSchema
 *  Author: psy_wombats
 *  Contact: psy_wombats@wombatrpgs.net
 */
package net.wombatrpgs.mgneschema.audio;

import net.wombatrpgs.mgneschema.audio.data.AudioMDO;
import net.wombatrpgs.mgns.core.Annotations.Path;

/**
 * MDO for musicz. I don't think it means anything.
 */
@Path("audio/")
public class MusicMDO extends AudioMDO {

}
